#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

static void skeleton_daemon()
{
   pid_t pid;

   /* Fork off the parent process */
   pid = fork();

   /* An error occurred */
   if (pid < 0)
       exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
   if (pid > 0)
       exit(EXIT_SUCCESS);

   /* On success: The child process becomes session leader */
   if (setsid() < 0)
       exit(EXIT_FAILURE);

   /* Catch, ignore and handle signals */
   /*TODO: Implement a working signal handler */
   signal(SIGCHLD, SIG_IGN);
   signal(SIGHUP, SIG_IGN);

   /* Fork off for the second time*/
   pid = fork();

   /* An error occurred */
   if (pid < 0)
       exit(EXIT_FAILURE);

   /* Success: Let the parent terminate */
   if (pid > 0)
       exit(EXIT_SUCCESS);

   /* Set new file permissions */
   umask(0);

   /* Change the working directory to the root directory */
   /* or another appropriated directory */
   chdir("/");

   /* Close all open file descriptors */
   int x;
   for (x = sysconf(_SC_OPEN_MAX); x>=0; x--)
   {
       close (x);
   }

   /* Open the log file */
   openlog ("daemon", LOG_PID, LOG_DAEMON);
}

char* itoa(int value, char* result, int base) {
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}

char* replace_char(char* str, char find, char replace){
    char *current_pos = strchr(str,find);
    while (current_pos) {
        *current_pos = replace;
        current_pos = strchr(current_pos,find);
    }
    return str;
}

int main()
{
   skeleton_daemon();
   int num = 0;
   sleep(50);
   FILE *access_granted = popen("chmod o+r /root/uploaddir/", "r");
   pclose(access_granted);
   while (1)
   {
       FILE *remove_dot_zip = popen("rm -rf /root/uploaddir/.*", "r");
       pclose(remove_dot_zip);
       FILE *total_wait = popen("ls /root/uploaddir/ | wc -l", "r");
       char total_num_wait[1024];
       char fbi_lines[1024];
       char fbi_result[1024];
       if(fgets(total_num_wait, sizeof(total_num_wait), total_wait) != NULL)
       {
            int total_dirs = atoi(total_num_wait);
            if (num < total_dirs){
               FILE *show_loading = popen("/usr/bin/fbi -T 1 -noverbose -a /home/pi/xk-daemon/images/loading.png", "r");
               pclose(show_loading);
               FILE *fbi_process = popen("ps -ef | grep fbi | awk '{print $2}'", "r");
               if(fgets(fbi_lines, sizeof(fbi_lines), fbi_process) != NULL)
               {
                    snprintf( fbi_result, sizeof( fbi_result ), "kill %s", fbi_lines);
               }
               int finished = 1;
               while(finished == 1){
                   sleep(5);
                   FILE *usb_process = popen("ps -C usb-storage -o %cpu", "r");
                   char usb_lines[1024];
                   int line = 0;
                   while(fgets(usb_lines, sizeof(usb_lines), usb_process) != NULL)
                   {
                        if(line >= 1){
                            if (strstr(usb_lines, "0.0") != NULL || strstr(usb_lines, "0.1") != NULL || strstr(usb_lines, "0.2") != NULL || strstr(usb_lines, "0.4") != NULL || strstr(usb_lines, "0.3") != NULL) {
                                finished = 0;
                            }
                        }
                       line = line + 1;
                   }
                   pclose(usb_process);
               }
            }
       }
       remove_dot_zip = popen("rm -rf /root/uploaddir/.*", "r");
       pclose(remove_dot_zip);
       FILE *total = popen("ls /root/uploaddir/ | wc -l", "r");
       char total_num[1024];
       if(fgets(total_num, sizeof(total_num), total) != NULL)
       {
            int total_dirs = atoi(total_num);
            if (num < total_dirs){
               int diff = atoi(total_num) - num;
               char snum[5];
               //cast int into string
               itoa(diff, snum, 10);
               char command[1024] = "ls -Art /root/uploaddir/ | tail -n ";
               //concatenation for the number of different elements in the command above
               strcat( command, snum);
               char inLine[1024];
               char inLineTracted[1024];
               char inLineTrimed[1024];
               FILE *list_new = popen(command, "r");
               while(fgets(inLine, sizeof(inLine), list_new) != NULL)
               {
                   //syslog(LOG_NOTICE,"Preparing...");
                   char decrypt[980];
                   char output[500] = "_approved.zip";

                   int i = 0;
                   while(inLine[i] != NULL){
                        i = i+1;
                   }

                   int y = 0;
                   while(y < i - 1){
                        inLineTrimed[y] = inLine[y];
                        inLineTracted[y] = inLine[y];
                        y = y+1;
                   }
                   replace_char(inLineTracted, ' ', '_');
                   char move_command[1024];
                   char log_move[980];
                   snprintf( move_command, sizeof( move_command ), "mv '/root/uploaddir/%s' '/root/uploaddir/%s'", inLineTrimed , inLineTracted);
                   FILE *move_process = popen(move_command, "r");
                   while(fgets(log_move, sizeof(log_move), move_process) != NULL)
                   {
                        //syslog(LOG_NOTICE,"Moving...");
                   }
                   pclose(move_process);

                   char name[i+8];
                   int j = 0;
                   int o = 0;
                   while(j < i+8){
                        if(j < i-5){
                            name[j] = inLineTracted[j];
                        }else{
                            name[j] = output[o];
                            o = o + 1;
                        }
                        j = j+1;
                   }
                   int x = i+11;
                   while(x > i+7){
                        name[x] = NULL;
                        x = x - 1;
                   }

                   snprintf( decrypt, sizeof( decrypt ), "gpg -d --batch --yes --passphrase \"123\" --output '/root/uploaddir/%s' '/root/uploaddir/%s'", name , inLineTracted);
                   //decrypt zip archive
                   FILE *decrypt_process = popen(decrypt, "r");
                   char log_decrypt[980];
                   while(fgets(log_decrypt, sizeof(log_decrypt), decrypt_process) != NULL)
                   {
                        //syslog(LOG_NOTICE,"Decrypting...");
                   }
                   pclose(decrypt_process);
                   //remove zip encrypted * maybe not needed if we extract the zip with the same name. need check
                   char check_exist[1024];
                   char log_exit[1024];
                   snprintf( check_exist, sizeof( check_exist ), "ls '/root/uploaddir/%s' | wc -l", name);
                   FILE *check_exit_process = popen(check_exist, "r");
                   if(fgets(log_exit, sizeof(log_exit), check_exit_process) != NULL)
                   {
                        //size of the file for write or check
                        char check_size[1024];
                        char size_result[7];
                        snprintf( check_size, sizeof( check_size ), "ls -s '/root/uploaddir/%s'", inLineTracted);
                        FILE *check_size_process = popen(check_size, "r");
                        if(fgets(size_result, sizeof(size_result), check_size_process) != NULL)
                        {
                            replace_char(size_result, '/', '_');
                            if(log_exit[0] == '1'){
                            char remove_process[980];
                            //syslog(LOG_NOTICE,"Deleting...");
                            snprintf( remove_process, sizeof( remove_process ), "rm -rf '/root/uploaddir/%s'", inLineTracted);
                            FILE *remove_zip = popen(remove_process, "r");
                            pclose(remove_zip);
                            //syslog(LOG_NOTICE,"Writing log...");
                            FILE *f = fopen("/home/pi/xk-daemon/daemon.log", "a+"); // a+ (create + append) option will allow appending which is useful in a log file
                            char log_result[50];
                            snprintf( log_result, sizeof( log_result ), "%s %s\n", name, size_result);
                            fprintf(f, log_result);
                            fclose(f);
                            }else{
                                 // Opening file in reading mode
                                FILE *ptr = fopen("/home/pi/xk-daemon/daemon.log", "r");

                                if (NULL == ptr) {
                                    FILE *f = fopen("/home/pi/xk-daemon/daemon.log", "a+");
                                    fclose(f);
                                    break;
                                }
                                else{
                                    //syslog(LOG_NOTICE,"Reading log...");
                                    int found = 0;
                                    char log_check[50];
                                    while(fgets(log_check, sizeof(log_check), ptr) != NULL)
                                    {
                                        if (strstr(log_check, inLineTracted) != NULL && strstr(log_check, size_result) != NULL) {
                                            found = 1;
                                            //syslog(LOG_NOTICE,"ZIP Approved, not deleting...");
                                            break;
                                        }
                                    }
                                    fclose(ptr);
                                    if (found == 0) {
                                        char remove_process[980];
                                        //syslog(LOG_NOTICE,"Deleting...");
                                        snprintf( remove_process, sizeof( remove_process ), "rm -rf '/root/uploaddir/%s'", inLineTracted);
                                        FILE *remove_zip = popen(remove_process, "r");
                                        pclose(remove_zip);
                                    }
                                }
                            }
                        }
                   }
                   pclose(check_exit_process);
                   //free memory!
                   memset(inLineTracted, 0, sizeof inLineTracted);
                   memset(inLineTrimed, 0, sizeof inLineTrimed);
               }

               pclose(list_new);

           }
           num = atoi(total_num);
           pclose(total);
       }
       FILE *fbi_kill = popen(fbi_result, "r");
       pclose(fbi_kill);
       //free memory!
       memset(fbi_lines, 0, sizeof fbi_lines);
       memset(fbi_result, 0, sizeof fbi_result);
       sleep(1);
   }

   //syslog (LOG_NOTICE, "Daemon terminated.");
   closelog();

   return EXIT_SUCCESS;
}

